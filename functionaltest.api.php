<?php

/**
 * @file
 * An example for using the DrupalFunctionalTestCase class.
 */

class MyWebsiteTestCase extends DrupalFunctionalTestCase {

  /**
   * Node structure definitions.
   *
   * These are templates that allow to easily create nodes during functional
   * testing. Each template typically contains the field names of all required
   * fields. This allows to quickly create a basic node:
   *
   * $node = $this->createNode('page');
   */

  /**
   * Node add form structure of a basic page.
   */
  private function _nodestructure_page() {
    $type = 'page';
    $struct = array();
    $struct['title'] = 'Test-' . $type . '-' . $this->randomName(16);
    $struct['body[und][0][value]'] = $this->randomParagraph(64);

    return $struct;
  }

  /**
   * Node add form structure of an event node.
   */
  private function _nodestructure_event() {
    $type = 'event';
    $struct = array();
    $struct['title'] = 'Test-' . $type . '-' . $this->randomName(16);
    $struct['body[und][0][value]'] = $this->randomParagraph(32);
    $struct['body[und][0][summary]'] = $this->randomParagraph(16);
    $struct['files[field_event_image_und_0]'] = $this->randomImagePath();
    $struct['field_event_date[und][0][value][date]'] = date('d/m/Y', rand(0, time()));
    $struct['field_event_link[und][0][url]'] = $this->randomUrl();
    $struct['field_event_tags[und]'] = $this->randomTerm('tags')->tid;

    return $struct;
  }

  /**
   * Term structure definitions.
   */

  /**
   * Basic term structure, only containing a name and description.
   */
  private function _termstructure_basic() {
    $structure = array();
    $structure['name'] = $this->randomName();
    $structure['description[value]'] = $this->randomParagraph();
    return $structure;
  }

  /**
   * Term structure for the blogs vocabulary.
   *
   * @return array
   *   The term structure for a term in the blogs vocabulary.
   */
  private function _termstructure_blogs() {
    $structure = $this->_termstructure_basic();
    $structure['files[field_blogs_image_und_0]'] = $this->randomImagePath();
    $structure['field_blogs_author[und][0][value]'] = $this->randomName();
    $structure['files[field_blogs_banner_und_0]'] = $this->randomImagePath();
    return $structure;
  }

  /**
   * Term structure for the categories vocabulary.
   *
   * @return array
   *   The term structure for a term in the categories vocabulary.
   */
  private function _termstructure_categories() {
    return $this->_termstructure_basic();
  }

  /**
   * Add your own helper assert methods.
   */

  /**
   * Pass if a link containing a given href (exact) is found.
   *
   * Drupal's default assertLinkByHref() will also match on partial hrefs. This
   * function will only assert on exact matches.
   *
   * @param $href
   *   The full value of the 'href' attribute of the anchor tag.
   * @param $index
   *   Link position counting from zero.
   * @param $message
   *   Message to display.
   * @param $group
   *   The group this message belongs to, defaults to 'Other'.
   *
   * @return
   *   TRUE if the assertion succeeded, FALSE otherwise.
   *
   * @see assertLinkByHref()
   */
  protected function assertLinkByExactHref($href, $index = 0, $message = '', $group = 'Other') {
    $links = $this->xpath('//a[@href=:href]', array(':href' => $href));
    $message = ($message ? $message : t('Link containing href %href found.', array('%href' => $href)));
    return $this->assert(isset($links[$index]), $message, $group);
  }

  /**
   * Pass if a link containing a given exact href is not found.
   *
   * Drupal's default assertNoLinkByHref() will also take partial hrefs into
   * account. This function will only assert when the exact href is not found.
   *
   * @param $href
   *   The full or partial value of the 'href' attribute of the anchor tag.
   * @param $message
   *   Message to display.
   * @param $group
   *   The group this message belongs to, defaults to 'Other'.
   *
   * @return
   *   TRUE if the assertion succeeded, FALSE otherwise.
   *
   * @see assertNoLinkByHref()
   */
  protected function assertNoLinkByExactHref($href, $message = '', $group = 'Other') {
    $links = $this->xpath('//a[@href=:href]', array(':href' => $href));
    $message = ($message ? $message : t('No link containing href %href found.', array('%href' => $href)));
    return $this->assert(empty($links), $message, $group);
  }

  /**
   * Helper methods.
   *
   * Add methods that are specific to your website and can be used by the tests.
   */

  /**
   * Creates a tree of nodes containing child nodes.
   *
   * In this example we have a parent node (TV Show) and a number of child nodes
   * which are related to the parent node using node references. This function
   * generates a node of each type and returns this as an array.
   *
   * @see http://drupal.org/project/references
   *
   * @param array $options
   *   An array containing options to pass to the generated nodes, keyed by node
   *   type. See $this->createNode().
   *
   * @return array
   *   An array, keyed on node type, containing a TV Show node with referring
   *   nodes Actor, Advertisement, Broadcast, Clip, Episode, Season.
   */
  function createTvShowTree($options = array()) {
    // An array of nodes to generate, with the required node relations. This is
    // ordered so all required relations are generated before they are needed.
    $types = array(
      'tvshow' => array(),
      'actor' => array(
        'tvshow' => 'field_actor_tvshow[und][0][nid]',
      ),
      'advertisement' => array(
        'tvshow' => 'field_advertisement_tvshow[und][0][nid]',
      ),
      'season' => array(
        'tvshow' => 'field_season_tvshow[und][0][nid]',
      ),
      'episode' => array(
        'season' => 'field_episode_season[und][0][nid]',
        'tvshow' => 'field_episode_tvshow[und][0][nid]',
      ),
      'broadcast' => array(
        'episode' => 'field_broadcast_episode[und][0][nid]',
        'tvshow' => 'field_broadcast_tvshow[und][0][nid]',
      ),
      'clip' => array(
        'actor' => 'field_clip_actor[und][0][nid]',
        'tvshow' => 'field_clip_tvshow[und][0][nid]',
      ),
    );

    $tree = array();

    // Loop through the defined types, create nodes of each required relation
    // and finally create a node that contains these relations.
    foreach ($types as $type => $relations) {
      $type_options = array();
      // Generate node relation options.
      foreach ($relations as $relation_type => $field) {
        $type_options[$field] = '[nid:' . $tree[$relation_type]->nid . ']';
      }
      // Merge in default options if these are given.
      if (!empty($options[$type])) {
        $type_options = $type_options + $options[$type];
      }
      $tree[$type] = $node = $this->createNode($type, $type_options);
      $result = (is_object($node) && !empty($node->nid) && isset($node->type) && $node->type == $type);
      $this->assertTrue($result, 'Created a node of type "' . $type . '" containing relations to ' . implode(', ', array_keys($relations)) . '.');
    }

    return $tree;
  }
}
