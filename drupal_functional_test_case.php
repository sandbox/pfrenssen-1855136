<?php

/**
 * @file
 * Some useful methods for functional testing of websites.
 */
class DrupalFunctionalTestCase extends DrupalRemoteTestCase {

  public function tearDown() {
    $this->cleanupUsers();
    $this->cleanupTerms();
    parent::tearDown();
  }

  /**
   * Log in a user with the internal browser.
   *
   * This differs from the core drupalLogin() method in that it checks that the
   * body element has the class 'logged-in', rather than checking for the
   * presence of the text 'Log out' on the page.
   *
   * @param $user
   *   User object representing the user to log in.
   */
  protected function drupalLogin(stdClass $user) {
    if ($this->loggedInUser) {
      $this->drupalLogout();
    }

    $edit = array(
      'name' => $user->name,
      'pass' => $user->pass_raw
    );
    $this->drupalPost('user', $edit, t('Log in'));

    // The login was successful if the class "logged-in" is present on the body
    // element.
    $pass = $this->assertPattern('/<body[^>]+class="[^(>|")]*logged-in[^(>|")]*"[^>]*>/', format_string('User %name successfully logged in.', array('%name' => $user->name)));

    if ($pass) {
      $this->loggedInUser = $user;
    }
  }

  /**
   * Retrieves a Drupal path or an absolute path.
   *
   * This differs from the core drupalGet() method in that it checks every
   * page for the presence of error messages and duplicate HTML element ids.
   */
  protected function drupalGet($path, array $options = array(), array $headers = array()) {
    $html = parent::drupalGet($path, $options, $headers);
    if (!$this->assertNoRaw('<h2 class="element-invisible">' . t('Error message') . '</h2>', t('No error messages present on the page.'))) {
      $matches = array();
      $error = preg_match('|<h2 class="element-invisible">' . t('Error message') . '</h2>[\s]?(.*?)[\s]?</div>|s', $this->content, $matches);
      $this->fail(check_plain(print_r($matches[1], TRUE)));
    }
    $this->assertNoDuplicateIds(t('No duplicate ids found on the page.'));
    return $html;
  }

  /**
   * Create a node of a given content type.
   *
   * @param string $contenttype
   *   The content type of the node.
   * @param array $options
   *   An array of options that will override the default node structure.
   *
   * @return object
   *   The node object that was created.
   */
  public function createNode($contenttype = NULL, $options = array()) {
    // Get the predefined node edit form structure.
    $structure = $this->getNodeStructure($contenttype);
    // Allow to override certain options.
    $structure = array_merge($structure, $options);

    return $this->saveStructureNode($contenttype, $structure);
  }

  /**
   * Create the node structure of a given content type.
   *
   * This function delegates the creation of the structure to a helper function
   * like _nodestructure_article().
   *
   * @param string $contenttype
   *   The machine name of the content type that will be used to create the
   *   node structure.
   *
   * @return array
   *   The node structure that was created.
   */
  public function getNodeStructure($contenttype) {
    $function_name = str_replace('-', '_', $contenttype);
    $function_name = '_nodestructure_' . $function_name;
    if (method_exists($this, $function_name)) {
      $struct = $this->{$function_name}();
    }
    else {
      throw new Exception('The node structure definition method ' . $function_name . '() does not exist in ' . __CLASS__ . '.');
    }
    return $struct;
  }

  /**
   *  Saves a structure as a node.
   */
  public function saveStructureNode($contenttype, $structure) {
    $this->drupalGet('node/add/' . strtr($contenttype, '_', '-'));
    $this->assertResponse(200, 'Logged in user ' . $this->loggedInUser->name . ' is allowed to create node of type ' . $contenttype . '.');
    $this->drupalPost(NULL, $structure, t('Save'));
    $response = curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
    $this->assertResponse(200, t('Received HTTP response "' . $response . '" when saving a node of type %type with title %title from the supplied node structure.', array('%type' => $contenttype, '%title' => $structure['title'])));

    // Attempt to find the node that has just been created.
    // This will fail if a destination has been set, or some form of form redirect.
    // @todo Cover these exceptions.
    if ($node = $this->currentPathtoNode()) {
      $this->nodes[$node->nid] = $node;
    }
    elseif ($node = $this->getNodeByTitle($structure['title'])) {
      $this->nodes[$node->nid] = $node;
    }
    else {
      $this->fail("Find the correct node... You've hit an edge case and you'll need to extend the testsuite!");
    }
    $this->assertTrue($node, t('Successfully saved a node of type %type with title %title from the supplied node structure.', array('%type' => $contenttype, '%title' => $structure['title'])));

    // Reset the statically cached path alias.
    drupal_clear_path_cache('node/' . $node->nid);

    // Allow to perform additional actions after saving a node.
    $this->saveStructureNodePostSave($contenttype, $structure, $node);

    return $node;
  }

  /**
   * Perform additional actions after saving a node.
   */
  protected function saveStructureNodePostSave($contenttype, $structure, $node) {
  }

  /**
   * Gets the node from current url.
   *
   * @return object
   *   Node
   */
  protected function currentPathtoNode() {
    $path = parse_url($this->getUrl());
    $path = drupal_get_normal_path(trim($path['path'], '/'));
    $path = explode('/', $path);
    if ($path[0] != 'node') {
      return FALSE;
    }
    $nid = $path[1];
    if (is_numeric($nid)) {
      return FALSE;
    }
    $node = node_load($nid);
    return $node;
  }

  /**
   * Create a term for a given vocabulary.
   *
   * @param array $vocabulary
   *   The vocabulary to which the term belongs.
   * @param array $options
   *   An array of options that will override the default term structure.
   *
   * @return object
   *   The term object that was created.
   */
  public function createTerm($vocabulary, $options = array()) {
    $structure = $this->getTermStructure($vocabulary);
    $structure = array_merge($structure, $options);
    $term = $this->saveStructureTerm($vocabulary, $structure);

    return $term;
  }

  /**
   * Get the structure for a term for a given vocabulary.
   *
   * This function delegates the creation of the structure to a helper function
   * like _termstructure_tags().
   *
   * @param array $vocabulary
   *   The vocabulary to which the term belongs.
   *
   * @return array
   *   The term structure for the term object.
   */
  public function getTermStructure($vocabulary) {
    $function_name = str_replace('-', '_', $vocabulary->machine_name);
    $function_name = '_termstructure_' . $function_name;
    if (method_exists($this, $function_name)) {
      $struct = $this->{$function_name}();
    }
    else {
      throw new Exception('The vocabulary structure definition method ' . $function_name . '() does not exist in ' . __CLASS__ . '.');
    }
    return $struct;
  }

  /**
   * Saves the term to the vocabulary and with the structure which are given as parameters.
   *
   * @param array $vocabulary
   *   The vocabulary to which the term belongs.
   * @param array $structure
   *   The structure of the term.
   *
   * @return object
   *   The term object which got saved.
   */
  public function saveStructureTerm($vocabulary, $structure) {
    $this->drupalPost('admin/structure/taxonomy/' . $vocabulary->machine_name . '/add', $structure, t('Save'));
    $term = $this->getLastUpdatedTerm($structure['name'], $vocabulary->machine_name);
    $this->assertText(strip_tags(t('Created new term %term.', array('%term' => $term->name))));
    $this->terms[$term->tid] = $term;

    // Reset the statically cached path alias.
    drupal_clear_path_cache('taxonomy/term/' . $term->tid);

    // Allow to perform additional actions after saving a taxonomy term.
    $this->saveStructureTermPostSave($vocabulary, $structure, $term);

    return $term;
  }

  /**
   * Perform additional actions after saving a taxonomy term.
   */
  protected function saveStructureTermPostSave($vocabulary, $structure, $term) {
  }

  /**
   * Create a new user of the specified role and log in as this user.
   * Switch user if a user is already logged in.
   *
   * @todo
   * This function does two things which is a Code Smell™. The only thing that
   * this function offers which can not be done very easily with drupalLogin()
   * and drupalCreateUser() is that it assigns a role to the created user.
   * There are two solutions:
   * - Refactor into a function setUserRole($user, $role).
   * - Refactor into a function that extends drupalCreateUser() but allows to
   *   pass in a role as well as the permissions from the original function.
   *
   * @param string $role
   *   The machine name of the user role to which the user should belong.
   * @param array $options
   *   An array of optional fields to be saved in the user object.
   *
   * @return object
   *   A fully loaded user object.
   */
  public function userCreate($role = 'anonymous user', $options = array()) {
    $user = $this->drupalCreateUser(array());
    if (!$rid = db_query("SELECT rid FROM {role} WHERE name = :name", array(':name' => $role))->fetchColumn(0)) {
      throw new exception('Unknown user role passed to userCreate().');
    }
    $user->roles = array();
    $user->roles[$rid] = $role;
    $edit['roles'] = $user->roles;
    $edit = array_merge($edit, $options);
    $this->privileged_user = user_save($user, $edit);
    $this->drupalLogin($user);
    $this->users[$user->uid] = $user;
    return $user;
  }

  public function deleteNode($nid) {
    $node = node_load($nid, NULL, TRUE);
    $this->verbose('Deleting node "' . $node->title . '" (type: ' . $node->type . ' nid: ' . $node->nid . ').');
    $this->drupalGet('node/' . $node->nid . '/delete');
    $this->assertResponse(200, 'User has permission to delete node "' . $node->title . '" (type: ' . $node->type . ' nid: ' . $node->nid . ').');
    $this->assertRaw(t('Are you sure you want to delete %title?', array('%title' => $node->title)), 'Delete question found');
    $this->drupalPost("node/$node->nid/delete", '', t('Delete'));
  }

  public function deleteTerm($tid) {
    $this->drupalPost("taxonomy/term/" . $tid . "/edit", '', t('Delete'));
    // Multistep taxonomy dele form. So we can't press the second delete button.
    $this->assertResponse(200, 'User is allawed to the page');
    taxonomy_term_delete($tid);
  }

  /**
   * Clean up the users that were created during the test run.
   */
  private function cleanupUsers() {
    if (isset($this->users)) {
      foreach ($this->users as $key => $user) {
        user_delete($user->uid);
        unset($this->users[$key]);
        $this->pass('Deleted test user ' . $user->uid . ':' . $user->name);
      }
    }
    $this->cleanupRoles();
  }

  /**
   * Clean up the user roles that were created during the test run.
   */
  private function cleanupRoles() {
    if (isset($this->roles)) {
      foreach ($this->roles as $key => $role) {
        user_role_delete($role);
        unset($this->roles[$key]);
        $this->pass('Deleted test user role ' . $role);
      }
    }
  }

  /**
   * Clean up the terms that were created during the test run.
   */
  private function cleanupTerms() {
    if (isset($this->terms)) {
      foreach ($this->terms as $key => $term) {
        taxonomy_term_delete($term->tid);
        unset($this->terms[$key]);
        $this->pass('Deleted test taxonomy term' . $term->tid . ':' . $term->name);
      }
    }
  }

  /**
   * Internal helper function; Create a role with specified permissions.
   *
   * @param $permissions
   *   Array of permission names to assign to role.
   * @param $name
   *   (optional) String for the name of the role.  Defaults to a random string.
   * @return
   *   Role ID of newly created role, or FALSE if role creation failed.
   */
  protected function drupalCreateRole(array $permissions, $name = NULL) {
    // Generate random name if it was not passed.
    if (!$name) {
      $name = $this->randomName();
      $this->roles[] = $name;
    }

    // Check the all the permissions strings are valid.
    if (!$this->checkPermissions($permissions)) {
      return FALSE;
    }

    // Create new role.
    $role = new stdClass();
    $role->name = $name;
    user_role_save($role);
    user_role_grant_permissions($role->rid, $permissions);

    $this->assertTrue(isset($role->rid), t('Created role of name: @name, id: @rid', array('@name' => $name, '@rid' => (isset($role->rid) ? $role->rid : t('-n/a-')))), t('Role'));
    if ($role && !empty($role->rid)) {
      $query = db_select('role_permission', 'r')->condition('rid', $role->rid);
      $count = $query->countQuery()->execute()->fetchField();
      $this->assertTrue($count == count($permissions), t('Created permissions: @perms', array('@perms' => implode(', ', $permissions))), t('Role'));
      return $role->rid;
    }
    else {
      return FALSE;
    }
  }

  public function getNodeByTitle($unique_title) {
    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));
    $query->condition('n.title', $unique_title, '=');
    $nid = $query
        ->execute()
        ->fetchCol(0);
    $node = node_load($nid[0]);
    $this->assertTrue($node, t('Found a node with title %title', array('%title' => $unique_title)));
    return $node;
  }

  private function getLastUpdatedTerm($unique_title, $content_type) {
    $query = db_select('taxonomy_term_data', 'ttd');
    $query->fields('ttd', array('tid'));
    $query->join('taxonomy_vocabulary', 'tv', 'tv.vid = ttd.vid');
    $query->condition('ttd.name', $unique_title, '=');
    $query->condition('tv.machine_name', $content_type, '=');
    $result = $query->execute();

    $tid = $result->fetchField();
    $term = taxonomy_term_load($tid);
    return $term;
  }

  /**
   * Generates a random paragraph with a given number of words.
   *
   * @param int $words
   *   The number of words to generate. Defaults to 32.
   * @param int $type
   *   0: return a plain text paragraph.
   *   1: return a paragraph wrapped in <p> elements.
   *   2: return a paragraph terminated with a <br /> element.
   *
   * @return string
   *   A random paragraph of text.
   */
  public function randomParagraph($words = 32, $type = 0) {
    // This requires the devel module to be present.
    if (module_load_include('inc', 'devel', 'devel_generate/devel_generate')) {
      return devel_create_para($words, $type);
    }
    // Fall back to a random string and warn if devel is missing.
    else {
      $this->error('The devel module is needed to generate random paragraphs.');
      return $this->randomName();
    }
  }

  /**
   * Returns the path of a random image from the module's files/images folder.
   *
   * @param string $imagefolder
   *   A folder containing test images, relative to the Drupal root folder.
   *
   * @return string
   *   A system path to a random image file.
   */
  public function randomImagePath($imagefolder) {
    $images = glob($imagefolder . '/*.{gif,jpg,jpeg,png}', GLOB_BRACE);
    $file = array_rand($images);
    return realpath($images[$file]);
  }

  /**
   * Returns a random URL from a whitelist.
   *
   * @return string
   *   A URL.
   */
  public function randomUrl() {
    $urls = array(
      'http://www.google.be/',
      'http://www.facebook.com/',
      'http://www.google.com/',
      'http://www.youtube.com/',
      'http://www.live.com/',
      'http://www.wikipedia.org/',
      'http://www.yahoo.com/',
      'http://www.linkedin.com/',
      'http://www.twitter.com/',
      'http://www.hln.be/',
      'http://www.msn.com/',
      'http://www.google.fr/',
      'http://www.ebay.be/',
      'http://www.standaard.be/',
      'http://www.nieuwsblad.be/',
      'http://www.immoweb.be/',
      'http://www.wordpress.com/',
      'http://www.telenet.be/',
      'http://www.dhnet.be/',
      'http://www.amazon.com/',
      'http://www.microsoft.com/',
    );
    $key = array_rand($urls);
    return $urls[$key];
  }

  /**
   * Retrieves a random taxonomy term from a given vocabulary.
   *
   * @param string $machine_name
   *   A vocabulary machine name.
   *
   * @return object
   *   A random term from the requested vocabulary.
   */
  public function randomTerm($machine_name) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($machine_name);
    $terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, FALSE);
    $key = array_rand($terms);
    return taxonomy_term_load($terms[$key]->tid);
  }

  /**
   * Creates an array of image objects.
   *
   * @param int $amount
   * The amount of "images" that should be created.
   *
   * @return array
   * Returns the created "images".
   */
  protected function getImages($amount, $imagefolder) {
    $files = array();
    for ($i = 0; $i < $amount; $i++) {
      $filepath = $this->randomImagePath($imagefolder);

      $file = (object) array(
        'uid' => 1,
        'uri' => $filepath,
        'title' => $this->randomName(10),
        'filemime' => file_get_mimetype($filepath),
        'status' => 1,
      );

      // We save the file to the root of the files directory.
      $files[] = (array) file_copy($file, 'public://');
    }

    return $files;
  }

  /**
   * Takes a stream wrapper URI and returns an absolute URL.
   *
   * @param string $uri
   *   A stream wrapper URI (eg. 'public://myimage.jpg').
   *
   * @return string|FALSE
   *   The absolute URL for the resource, including protocol and domain
   *   (e.g. 'http://www.mysite.com/sites/default/files/myimage.jpg'), or FALSE
   *   if the given URI is invalid.
   */
  protected function getAbsoluteUrlFromUri($uri) {
    global $base_url;

    // Split out the scheme and target path.
    list($scheme, $target) = explode('://', $uri, 2);

    if (!file_stream_wrapper_valid_scheme($scheme)) {
      return FALSE;
    }

    $wrapper = file_stream_wrapper_get_instance_by_scheme($scheme);
    $wrapper->setUri($uri);

    // Remove erroneous leading or trailing, forward-slashes and backslashes.
    $target = trim($target, '\/');

    return $base_url . '/' . $wrapper->getDirectoryPath() . '/' . $target;
  }

  /**
   * Check that an image is present by checking the value of the image's src
   * attribute.
   *
   * @param type $src
   *   The source of the image to check for.
   *
   * @param int $index
   * @param string $message
   * @param string $group
   */
  protected function assertImageBySrc($src, $index = 0, $message = '', $group = 'Other') {
    $images = $this->xpath('//img[contains(@src, :src)]', array(':src' => $src));
    $message = ($message ? $message : t('Image with src %src found.', array('%src' => $src)));
    return $this->assert(isset($images[$index]), $message, $group);
  }

  /**
   * Check that an image is not present by checking the value of the image's
   * src attribute.
   *
   * @param type $src
   *   The source of the image to check for.
   *
   * @param int $index
   * @param string $message
   * @param string $group
   *
   * @return type
   */
  protected function assertNoImageBySrc($src, $index = 0, $message = '', $group = 'Other') {
    $images = $this->xpath('//img[contains(@src, :src)]', array(':src' => $src));
    $message = ($message ? $message : t('Image with src %src not found.', array('%src' => $src)));
    return $this->assert(!isset($images[$index]), $message, $group);
  }

  /**
   * Pass if an element with the given id is found.
   *
   * @param $id
   *   The id that should be found on an element.
   * @param $message
   *   Message to display.
   * @param $group
   *   The group this message belongs to.
   *
   * @return
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertElementById($id, $message = '', $group = 'Other') {
    $elements = $this->xpath('//*[@id=:id]', array(':id' => $id));
    if (!$message) {
      $message = format_string('An element with id @id is found on the page.', array('@id' => $id));
    }
    return $this->assertTrue(count($elements), $message, $group);
  }

  /**
   * Pass if an element with the given id is not found.
   *
   * @param $id
   *   The id that should not be found on an element.
   * @param $message
   *   Message to display.
   * @param $group
   *   The group this message belongs to.
   *
   * @return
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertNoElementById($id, $message = '', $group = 'Other') {
    $elements = $this->xpath('//*[@id=:id]', array(':id' => $id));
    if (!$message) {
      $message = format_string('An element with id @id is not found on the page.', array('@id' => $id));
    }
    return $this->assertFalse(count($elements), $message, $group);
  }
}
