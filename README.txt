Extends DrupalRemoteTestCase with a number of helpful methods for testing the
functionality of websites.

This depends on the Remote Test module, or the Simpletest contrib module.
@see http://drupal.org/sandbox/pfrenssen/1855054
@see http://drupal.org/project/simpletest
